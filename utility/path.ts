import { SchematicContext, Tree } from "@angular-devkit/schematics";
import { buildRelativePath, findModuleFromOptions, ModuleOptions } from "@schematics/angular/utility/find-module";
import { basename, dirname, normalize } from "@angular-devkit/core";

export function configurePath(schematic:Function, options:ModuleOptions) {
  return (host: Tree, context: SchematicContext) => {
    if(options.module) {
      let modulePath = findModuleFromOptions(host, options);

      if (modulePath) {
        let sourceDir:string = options.sourceDir || 'src';
        let path:string = options.path || 'app';
        options.path = normalize(buildRelativePath(`/${sourceDir}/${path}`, dirname(normalize(modulePath))));
      }
    }

    // enable "flat" install if it would create a directory with the same name as its parent
    if(options.path && options.hasOwnProperty('flat') && !options.flat && basename(normalize(options.path)) == options.name) {
      options.flat = true;
    }

    return schematic(options)(host, context);
  };
}