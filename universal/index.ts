/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
import { normalize, strings } from '@angular-devkit/core';
import {
  Rule,
  SchematicContext,
  SchematicsException,
  Tree,
  apply,
  chain,
  mergeWith,
  template,
  url,
} from '@angular-devkit/schematics';
import * as ts from 'typescript';
import { findNode, getDecoratorMetadata } from '@schematics/angular/utility/ast-utils';
import { InsertChange } from '@schematics/angular/utility/change';
import { AppConfig, getAppFromConfig, getConfig } from '@schematics/angular/utility/config';
import { findBootstrapModuleCall, findBootstrapModulePath } from '@schematics/angular/utility/ng-ast-utils';
import { Schema as UniversalOptions } from '@schematics/angular/universal/schema';


function updateConfigFile(options: UniversalOptions): Rule {
  return (host: Tree) => {
    const config = getConfig(host);
    const clientApp = getAppFromConfig(config, options.clientApp || '0');
    if (clientApp === null) {
      throw new SchematicsException('Client app not found.');
    }
    options.test = options.test || clientApp.test;

    const tsCfg = options.tsconfigFileName && options.tsconfigFileName.endsWith('.json')
      ? options.tsconfigFileName : `${options.tsconfigFileName}.json`;
    const testTsCfg = options.testTsconfigFileName && options.testTsconfigFileName.endsWith('.json')
      ? options.testTsconfigFileName : `${options.testTsconfigFileName}.json`;

    const serverApp: AppConfig = {
      ...clientApp,
      name: 'server',
      platform: 'server',
      root: options.root,
      outDir: options.outDir,
      index: options.index,
      main: options.main,
      test: options.test,
      tsconfig: tsCfg,
      testTsconfig: testTsCfg,
      polyfills: undefined,
    };
    if (!config.apps) {
      config.apps = [];
    }
    config.apps.push(serverApp);

    host.overwrite('/.angular-cli.json', JSON.stringify(config, null, 2));

    return host;
  };
}

function findBrowserModuleImport(host: Tree, modulePath: string): ts.Node {
  const moduleBuffer = host.read(modulePath);
  if (!moduleBuffer) {
    throw new SchematicsException(`Module file (${modulePath}) not found`);
  }
  const moduleFileText = moduleBuffer.toString('utf-8');

  const source = ts.createSourceFile(modulePath, moduleFileText, ts.ScriptTarget.Latest, true);

  const decoratorMetadata = getDecoratorMetadata(source, 'NgModule', '@angular/core')[0];
  const browserModuleNode = findNode(decoratorMetadata, ts.SyntaxKind.Identifier, 'BrowserModule');

  if (browserModuleNode === null) {
    throw new SchematicsException(`Cannot find BrowserModule import in ${modulePath}`);
  }

  return browserModuleNode;
}

function wrapBootstrapCall(options: UniversalOptions): Rule {
  return (host: Tree) => {
    const config = getConfig(host);
    const clientApp = getAppFromConfig(config, options.clientApp || '0');
    if (clientApp === null) {
      throw new SchematicsException('Client app not found.');
    }
    const mainPath = normalize(`/${clientApp.root}/${clientApp.main}`);
    let bootstrapCall: ts.Node | null = findBootstrapModuleCall(host, mainPath);
    if (bootstrapCall === null) {
      throw new SchematicsException('Bootstrap module not found.');
    }

    let bootstrapCallExpression: ts.Node | null = null;
    let currentCall = bootstrapCall;
    while (bootstrapCallExpression === null && currentCall.parent) {
      currentCall = currentCall.parent;
      if (currentCall.kind === ts.SyntaxKind.ExpressionStatement) {
        bootstrapCallExpression = currentCall;
      }
    }
    bootstrapCall = currentCall;

    const recorder = host.beginUpdate(mainPath);
    const beforeText = `document.addEventListener('DOMContentLoaded', () => {\n  `;
    const afterText = `\n});`;
    recorder.insertLeft(bootstrapCall.getStart(), beforeText);
    recorder.insertRight(bootstrapCall.getEnd(), afterText);
    host.commitUpdate(recorder);
  };
}

function addServerTransition(options: UniversalOptions): Rule {
  return (host: Tree) => {
    const config = getConfig(host);
    const clientApp = getAppFromConfig(config, options.clientApp || '0');
    if (clientApp === null) {
      throw new SchematicsException('Client app not found.');
    }
    const mainPath = normalize(`/${clientApp.root}/${clientApp.main}`);

    const bootstrapModuleRelativePath = findBootstrapModulePath(host, mainPath);
    const bootstrapModulePath = normalize(`/${clientApp.root}/${bootstrapModuleRelativePath}.ts`);

    const browserModuleImport = findBrowserModuleImport(host, bootstrapModulePath);
    const appId = options.appId;
    const transitionCall = `.withServerTransition({ appId: '${appId}' })`;
    const position = browserModuleImport.pos + browserModuleImport.getFullText().length;
    const transitionCallChange = new InsertChange(
      bootstrapModulePath, position, transitionCall);

    const transitionCallRecorder = host.beginUpdate(bootstrapModulePath);
    transitionCallRecorder.insertLeft(transitionCallChange.pos, transitionCallChange.toAdd);
    host.commitUpdate(transitionCallRecorder);
  };
}

function addDependencies(): Rule {
  return (host: Tree) => {
    const pkgPath = '/package.json';
    const buffer = host.read(pkgPath);
    if (buffer === null) {
      throw new SchematicsException('Could not find package.json');
    }

    const pkg = JSON.parse(buffer.toString());

    pkg.scripts['start'] = "rimraf dist-server && concurrently -k -n SERVER,BUILD,BUILD \"npm run server\" \"npm run build:client -- -w\" \"npm run build:server -- -w\"";
    pkg.scripts['build'] = "rimraf dist-server && concurrently -n SERVER,BUILD,BUILD \"npm run server\" \"npm run build:client -- -prod -sw\" \"npm run build:server\"";
    pkg.scripts['build:client'] = "ng build --vendorChunk --app client";
    pkg.scripts['build:server'] = "ng build -prod --output-hashing=none --app server";
    pkg.scripts['server'] = "ts-node-dev --respawn server.ts";
    //pkg.scripts['build:prerender'] = "npm run build && npm run generate:prerender";
    //pkg.scripts['generate:prerender'] = "cd dist && node prerender";
    //pkg.scripts['serve:prerender'] = "cd dist && http-server";

    const ngCoreVersion = pkg.dependencies['@angular/core'];
    pkg.dependencies['@angular/platform-server'] = ngCoreVersion;
    pkg.dependencies['@nguniversal/express-engine'] = '^5.0.0-beta.5';
    pkg.dependencies['@nguniversal/module-map-ngfactory-loader'] = '^5.0.0-beta.5';
    pkg.dependencies['express'] = '^4.16.2';
    pkg.dependencies['body-parser'] = '^1.18.2';
    pkg.dependencies['multer'] = '^1.3.0';
    pkg.dependencies['mysql'] = '^2.15.0';
    pkg.dependencies['reflect-metadata'] = '^0.1.12';
    pkg.dependencies['routing-controllers'] = '^0.7.6';
    pkg.dependencies['typedi'] = '^0.5.2';
    pkg.dependencies['typeorm'] = '^0.1.11';
    pkg.dependencies['typeorm-routing-controllers-extensions'] = '^0.1.0';
    pkg.dependencies['typeorm-typedi-extensions'] = '^0.1.1';

    pkg.devDependencies['@types/body-parser'] = '^1.16.8';
    pkg.devDependencies['@types/express'] = '^4.11.0';
    pkg.devDependencies['@types/multer'] = '^1.3.6';
    pkg.devDependencies['@types/node'] = '~9.4.0';
    pkg.devDependencies['child-process-promise'] = '^2.2.1';
    pkg.devDependencies['http-server'] = '^0.10.0';
    pkg.devDependencies['reflect-metadata'] = '^0.1.10';
    pkg.devDependencies['ts-node-dev'] = '~1.0.0-pre.9';
    pkg.devDependencies['concurrently'] = '^3.5.1';
    pkg.devDependencies['rimraf'] = '^2.6.2';

    host.overwrite(pkgPath, JSON.stringify(pkg, null, 2));

    return host;
  };
}

function updateGitignore(options: UniversalOptions): Rule {
  return (host: Tree) => {
    const ignorePath = normalize('/.gitignore');
    const buffer = host.read(ignorePath);
    if (buffer === null) {
      // Assumption is made that there is no git repository.
      return host;
    } else {
      const content = buffer.toString();
      host.overwrite(ignorePath, `${content}\n${options.outDir}`);
    }

    return host;
  };
}

export default function (options: UniversalOptions): Rule {
  return (host: Tree, context: SchematicContext) => {
    const templateSource = apply(url('./files'), [
      template({
        ...strings,
        ...options,
        stripTsExtension: (s: string) => { return s.replace(/\.ts$/, ''); },
      }),
    ]);

    return chain([
      mergeWith(templateSource),
      addDependencies(),
      updateConfigFile(options),
      wrapBootstrapCall(options),
      addServerTransition(options),
      updateGitignore(options),
    ])(host, context);
  };
}
