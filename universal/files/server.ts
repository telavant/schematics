import 'zone.js/dist/zone-node';
import 'reflect-metadata';
import { config } from './src/config/config';
import { enableProdMode } from '@angular/core';
import { augmentAppWithServiceWorker } from '@angular/cli/utilities/service-worker';
import { ngExpressEngine } from '@nguniversal/express-engine';
import { provideModuleMap } from '@nguniversal/module-map-ngfactory-loader';
import { Container } from 'typedi';
import { Connection, createConnection, useContainer as useOrmContainer } from 'typeorm';
import { useExpressServer, useContainer as useRoutingContainer } from 'routing-controllers';
import { Application as ExpressApplication, static as StaticRouter } from 'express';
import * as chokidar from 'chokidar';
import * as express from 'express';
import * as fs from 'fs';

const DIRS = {
  client: 'dist',
  server: 'dist-server',
};

let bundles = {};

function isReady():boolean {
  let ready:boolean = true;

  for(let key in DIRS) {
    if(DIRS.hasOwnProperty(key)) {
      try {
        let folder:string = DIRS[key];
        let file:string = fs.readdirSync(folder).filter(file => file.match(/^main\..*\.js$/)).shift();

        if (file.length) {
          bundles[folder] = './' + folder + '/' + file;
        } else {
          ready = false;
        }
      } catch(error) {
        ready = false;
      }
    }
  }

  return ready;
}

if(isReady()) {
  console.log("Starting server...");
  startServer();
} else {
  let watcher = chokidar.watch('.', {
    ignored: /(\.git|\.idea|src|e2e|node_modules)/,
  });

  watcher.on('ready', () => {
    console.log("Waiting for build...");
  }).on('add', path => {
    if(path.match(/main\..*\.js$/) && isReady()) {
      console.log("Build complete. Starting server...");
      watcher.close();
      startServer();
    }
  });
}

function startServer() {
  enableProdMode();

  // FIXME: remove this when running "ng build --watch" installs the service worker
  if (!fs.existsSync(`./${DIRS.client}/ngsw-worker.js`) || !fs.existsSync(`./${DIRS.client}/ngsw.json`)) {
    console.log("Installing service worker");
    augmentAppWithServiceWorker('.', './src', './' + DIRS.client, '/');
  }

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
  let { AppServerModuleNgFactory, LAZY_MODULE_MAP } = require('./' + DIRS.server + '/main.bundle.js');
  let server:ExpressApplication = express();

  server.engine('html', ngExpressEngine({
    bootstrap: AppServerModuleNgFactory,
    providers: [
      provideModuleMap(LAZY_MODULE_MAP),
    ],
  }));

  server.set('view engine', 'html');
  server.set('views', DIRS.client);

  // serve static content
  server.all(/\.(html|js|json|css|map|ico|gif|jpg|png)$/, StaticRouter(DIRS.client));

  // configure controller routes
  useExpressServer(express, {
    routePrefix: '/api',
    controllers: config.controllers,
    defaultErrorHandler: false,
  });

  // render everything else through ngExpressEngine
  server.use((req, res, next) => {
    if (!res.finished) {
      res.render('index', { async:true, req }, (err: Error, html: string) => {
        if(err instanceof Error) {
          console.error(`Error while rendering html: ${err.message}`);
        } else {
          // send output
          res.send(html);
        }
      });
    } else {
      next();
    }
  });

  try {
    useRoutingContainer(Container);
    useOrmContainer(Container);

    server.listen(config.http.port, () => {
      console.log(`Started ${config.environment} server at http://${config.http.host}:${config.http.port}`);

      if(config.db) {
        createConnection(config.db).then((connection: Connection) => {
          console.log(`Connected to ${config.db.type} database '${config.db.database}' in ${config.environment} environment`);
        });
      }
    });
  } catch (error) {
    console.error(`Error starting server: ${error}`);
  }
}
