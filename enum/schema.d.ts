/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */

import { Schema as BaseSchema } from '@schematics/angular/enum/schema';

export interface Schema extends BaseSchema {
  /**
   * Creates the enum in the specified module's directory.
   */
  module?: string;
}
