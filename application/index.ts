/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
import { strings } from '@angular-devkit/core';
import {
  Rule,
  SchematicContext,
  Tree,
  apply,
  chain,
  filter,
  mergeWith,
  move,
  noop,
  template,
  url, schematic,
} from '@angular-devkit/schematics';
import { Schema as ApplicationOptions } from './schema';

function minimalPathFilter(path: string): boolean {
  const toRemoveList: RegExp[] = [/e2e\//, /editorconfig/, /README/, /karma.conf.js/,
                                  /protractor.conf.js/, /test.ts/, /tsconfig.spec.json/,
                                  /tslint.json/, /favicon.ico/];

  return !toRemoveList.some(re => re.test(path));
}

export default function (options: ApplicationOptions): Rule {
  return (host: Tree, context: SchematicContext) => {
    const sourceDir = options.sourceDir || 'src';

    const templateSource = apply(url('./files'), [
      template({
        ...strings,
        ...options,
        sourcedir: sourceDir,
        selector: `${options.prefix}-root`,
        dot: '.',
      })
    ]);

    return chain([
      mergeWith(templateSource),
      options.inlineTemplate ? filter(path => !path.endsWith('.html')) : noop(),
      options.minimal ? filter(minimalPathFilter) : noop(),
      options.routing ? noop() : filter(path => !path.endsWith('-routing.module.ts')),
      options.skipGit ? filter(path => !path.endsWith('/__dot__gitignore')) : noop(),
      options.skipTests ? filter(path => !path.endsWith('.spec.ts')) : noop(),
      options.serviceWorker ? noop() : filter(path => !path.endsWith('/ngsw-config.json')),
      options.universal ? schematic('universal', { name: options.name, sourceDir: sourceDir }) : noop(),
      options.bootstrap ? schematic('bootstrap', { sourceDir: sourceDir }) : noop(),
      options.typeorm ? schematic('typeorm', {}) : noop(),
      move(options.directory),
    ])(host, context);
  };
}
