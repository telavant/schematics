/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */

import { Schema as BaseSchema } from '@schematics/angular/application/schema';

export interface Schema extends BaseSchema {
    /**
     * Enables Angular Universal support for the application.
     */
    universal?: boolean;
    /**
     * Enables Twitter Bootstrap support for the application.
     */
    bootstrap?: boolean;
    /**
     * Enables TypeORM support for the application.
     */
    typeorm?: boolean;
}
