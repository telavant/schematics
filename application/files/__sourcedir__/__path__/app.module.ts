import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';<% if (routing) { %>
import { AppRoutingModule } from './app-routing.module';<% } %><% if (serviceWorker) { %>
import { ServiceWorkerModule } from '@angular/service-worker';<% } %>
import { environment } from '#env/environment';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule<% if (routing) { %>,
    AppRoutingModule<% } %><% if (serviceWorker) { %>,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })<% } %>
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
