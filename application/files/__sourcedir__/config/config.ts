import { IConfig } from '../shared/config.interface';
import { environment } from '../environments/environment';

export const config:IConfig = {
  name: '<%= dasherize(name) %>',
  environment: environment.production ? 'production' : 'development',
  http: {
    host: 'localhost',
    port: 3000,
  },
/*
  db: {
    name: 'default',
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'dbuser',
    password: 'dbpass',
    database: 'dbname',
  },
*/
  controllers: [

  ],
};