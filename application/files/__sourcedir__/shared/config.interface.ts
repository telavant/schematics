import { ConnectionOptions as OrmConnectionOptions } from 'typeorm';

export interface HttpConnectionOptions {
  readonly host: string;
  readonly port: number;
}

export interface IConfig {
  readonly name: string;
  readonly environment: 'development' | 'staging' | 'production';
  readonly http: HttpConnectionOptions;
  readonly db?: OrmConnectionOptions;
  readonly controllers: Function[];
}
