/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */

export interface Schema {
  /**
   * The path of the source directory.
   */
  sourceDir?: string;
  /**
   * Name or index of related client app.
   */
  clientApp?: string;
  /**
   * The file extension to be used for style files.
   */
  styleext?: string;
  /**
   * The version of the Bootstrap library to use (3 or 4).
   */
  bootstrapVersion?: number;
}
