/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
import { normalize, strings } from '@angular-devkit/core';
import {
  Rule,
  SchematicContext,
  SchematicsException,
  Tree,
  apply,
  chain,
  mergeWith,
  template,
  url, noop, filter,
} from '@angular-devkit/schematics';
import { getAppFromConfig, getConfig } from '@schematics/angular/utility/config';
import { Schema as BootstrapOptions } from './schema';

function updateConfigFile(options: BootstrapOptions): Rule {
  return (host: Tree) => {
    const config = getConfig(host);

    if(options.styleext == 'css' || options.bootstrapVersion == 3) {
      const clientApp = getAppFromConfig(config, options.clientApp || '0');
      if (clientApp === null) {
        throw new SchematicsException('Client app not found.');
      }

      if(!clientApp.styles) {
        clientApp.styles = [];
      }

      clientApp.styles.push('../node_modules/bootstrap/dist/css/bootstrap.min.css');

      host.overwrite('/.angular-cli.json', JSON.stringify(config, null, 2));
    }

    return host;
  };
}

function addDependencies(options: BootstrapOptions): Rule {
  return (host: Tree) => {
    const pkgPath = '/package.json';
    const buffer = host.read(pkgPath);
    if (buffer === null) {
      throw new SchematicsException('Could not find package.json');
    }

    const pkg = JSON.parse(buffer.toString());

    pkg.dependencies['ngx-bootstrap'] = '^2.0.1';
    pkg.dependencies['bootstrap'] = options.bootstrapVersion == 4 ? '^4.0.0' : '^3.3.7';

    host.overwrite(pkgPath, JSON.stringify(pkg, null, 2));

    return host;
  };
}

function importStyles(options: BootstrapOptions): Rule {
  return (host: Tree) => {
    if(options.styleext == 'scss' && options.bootstrapVersion == 4) {
      const stylesPath = normalize(`/${options.sourceDir}/styles.scss`);
      const buffer = host.read(stylesPath);
      if (buffer === null) {
        throw new SchematicsException('Could not find styles.scss');
      }

      const content = buffer.toString() + "@import 'config/variables';\n" +
        "@import '../node_modules/bootstrap/scss/bootstrap';";

      host.overwrite(stylesPath, content);
    }

    return host;
  };
}


export default function (options: BootstrapOptions): Rule {
  return (host: Tree, context: SchematicContext) => {
    const templateSource = apply(url('./files'), [
      options.styleext == 'scss' ? noop() : filter(path => !path.endsWith('.scss')),
      template({
        utils: strings,
        ...options,
      }),
    ]);

    return chain([
      mergeWith(templateSource),
      addDependencies(options),
      updateConfigFile(options),
      importStyles(options),
    ])(host, context);
  };
}
