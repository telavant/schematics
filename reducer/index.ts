import { Rule } from '@angular-devkit/schematics';
import { Schema } from '@ngrx/schematics/src/reducer/schema';
import schematic from '@ngrx/schematics/src/reducer';
import { configurePath } from '../utility/path';

export default function(options: Schema): Rule {
  return configurePath(schematic, options);
}
