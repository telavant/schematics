/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
import {
  Rule,
  SchematicContext,
  SchematicsException,
  Tree,
  chain,
} from '@angular-devkit/schematics';

function addDependencies(): Rule {
  return (host: Tree) => {
    const pkgPath = '/package.json';
    const buffer = host.read(pkgPath);
    if (buffer === null) {
      throw new SchematicsException('Could not find package.json');
    }

    const pkg = JSON.parse(buffer.toString());

    pkg.dependencies['typeorm'] = '^0.1.11';
    pkg.dependencies['typedi'] = '^0.6.0';
    pkg.dependencies['typeorm-typedi-extensions'] = '^0.1.1';
    pkg.dependencies['routing-controllers'] = '^0.7.6';
    pkg.dependencies['typeorm-routing-controllers-extensions'] = '^0.1.0';

    host.overwrite(pkgPath, JSON.stringify(pkg, null, 2));

    return host;
  };
}

export default function (): Rule {
  return (host: Tree, context: SchematicContext) => {
    return chain([
      addDependencies(),
    ])(host, context);
  };
}
