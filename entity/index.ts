import { Rule } from '@angular-devkit/schematics';
import { Schema } from '@ngrx/schematics/src/entity/schema';
import schematic from '@ngrx/schematics/src/entity';
import { configurePath } from '../utility/path';

export default function(options: Schema): Rule {
  return configurePath(schematic, options);
}
