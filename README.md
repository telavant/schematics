# Telavant Schematics for Angular CLI

This is a module that provides a customized version of @schematic/angular to help us
reduce setup time for new projects. To use it, specify the package with the -c option
to `ng new`:

```bash
npm install -g @angular/cli
npm install -g @telavant/schematics
ng new -c @telavant/schematics projectname
```

The new project will be configured to use the @telavant/schematics collection
automatically.

## Changes from @angular/schematics

* Added `ng new --universal` to instruct the application generator to set up server-side
rendering during project creation if desired, versus doing it as a second step.

* Files will be created under their associated module (specified by the `-m` option),
resulting in a more natural file hierarchy.

* Classes, enums, and interfaces now have the `-m` option, though in their cases it's
only used to install the files in the same folder as the specified module.

* Changed defaults for the application generator to enable the routing and service
worker features, and sets the stylesheet extension to scss.

* Added `bootstrap` schematic for setting up Twitter Bootstrap.

* Added `typeorm` schematic for setting up TypeORM.

* Incorporated `@ngrx/schematics`. 

# Getting Started With Schematics

This repository is a basic Schematic implementation that serves as a starting point to create and publish Schematics to NPM.

### Testing

To test locally, install `@angular-devkit/schematics` globally and use the `schematics` command line tool. That tool acts the same as the `generate` command of the Angular CLI, but also has a debug mode.

Check the documentation with
```bash
schematics --help
```

### Unit Testing

`npm run test` will run the unit tests, using Jasmine as a runner and test framework.

### Publishing

To publish, simply do:

```bash
npm run build
npm publish
```

That's it!
 