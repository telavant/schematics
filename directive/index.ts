/**
* @license
* Copyright Google Inc. All Rights Reserved.
*
* Use of this source code is governed by an MIT-style license that can be
* found in the LICENSE file at https://angular.io/license
*/
import { Rule } from '@angular-devkit/schematics';
import { Schema } from '@schematics/angular/directive/schema';
import schematic from '@schematics/angular/directive';
import { configurePath } from '../utility/path';

export default function(options: Schema): Rule {
  return configurePath(schematic, options);
}
